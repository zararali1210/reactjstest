import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";

const EditUser = () => {
  let history = useHistory();
  const { id } = useParams();
  const [user, setUser] = useState({
    first_name: "",
    last_name: "",
    email: "",  
    status: "",
    role: "",
    type:1
  });
  const { first_name, last_name, email} = user;
  const onInputChange = e => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadUser();
  }, []);

  const onSubmit = async e => {
    e.preventDefault();
    user.type=1;
    await axios.post("http://localhost:5000/api/user/create", user);
    history.push("/");
  };

  /*Get record by Id*/

  const loadUser = async () => {
    const result = await axios.get(`http://localhost:5000/api/user/findOne?id=${id}`);
    setUser(result.data.result.response);
  };
  return (
    <div className="container">
      <div className="w-75 mx-auto shadow p-5">
        <h2 className="text-center mb-4">Update User</h2>
        <form onSubmit={e => onSubmit(e)}>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter First Name"
              name="first_name"
              value={first_name}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group">
          <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Last Name"
              name="last_name"
              value={last_name}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control form-control-lg"
              placeholder="Enter Your Email"
              name="email"
              value={email}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group">
          <select  className="form-control form-control-lg" type="role"   name="role" onChange={e => onInputChange(e)}>Select Role
          <option value="0">Admin</option>
          <option value="1">Role</option>
          </select>
          </div>

            <div className="form-group">
            <select className="form-control form-control-lg" type="Status"   name="status" onChange={e => onInputChange(e)}>Select Status
            <option value="0">In-Active</option>
            <option value="1">Active</option>
            </select>
          </div>
          <button className="btn btn-primary btn-block">Update User</button>
        </form>
      </div>
    </div>
  );
};

export default EditUser;
